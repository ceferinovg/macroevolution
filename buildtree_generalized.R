# map phylogenetic tree via squared-changed parsimony
# as in McArdle & Rodrigo 1994
# return tree length

buildtree.gen <- function (trees) {
  
  M<-matrix(c(0),ncol=(trees$Nnode+length(TrueTree$tip.label)),nrow=(trees$Nnode+length(TrueTree$tip.label)))
  
  M[trees$edge] <- 1
  ind_rev <- t(apply(trees$edge, 1,rev))
  M[ind_rev] <- 1
  
  MAI<-matrix(M[c((length(TrueTree$tip.label)+1):(trees$Nnode+length(TrueTree$tip.label))),
                c((length(TrueTree$tip.label)+1):(trees$Nnode+length(TrueTree$tip.label)))],
              ncol=trees$Nnode,nrow=trees$Nnode)
  
  MAI[MAI!=0]<--1
  
  diag(MAI)<-rowSums(M[c((length(TrueTree$tip.label)+1):(trees$Nnode+length(TrueTree$tip.label))),])
  
  Prevector<-rbind(c(1:length(TrueTree$tip.label)),matrix(M[c((length(TrueTree$tip.label)+1):(trees$Nnode+length(TrueTree$tip.label))),
                                              c(1:length(TrueTree$tip.label))],ncol=length(TrueTree$tip.label),
                                            nrow=trees$Nnode))
  
  nochange<-trees$tip.label
  
  Prevector[1,]<-as.numeric(replace(nochange,match(toupper(letters[c(1:length(TrueTree$tip.label))]),nochange),c(1:length(TrueTree$tip.label))))
  
  ones <- which(Prevector[-1,]==1, arr.ind = T)
  
  ones_val <- cbind(ones,values[Prevector[1,ones[,2]],])
  
  nas <- as.matrix(which(is.na(match(1:trees$Nnode,sort(unique(ones[,1]))))))
  
  nas <- cbind(nas, matrix(rep(0,(nrow(nas)*(ncol(values)+1))), ncol= ncol(values)+1))
  
  ones_val <- rbind(ones_val,nas)
  
  Y <- as.matrix(aggregate(ones_val[,-c(1:2)]~ones_val[,1], FUN = sum)[,-1])
  
  internalnodes<-solve(MAI,Y)
  
  Connections<-M[lower.tri(M)]<-0
  Connections<-which(M==1,arr.ind=TRUE)
  AllValues <- as.matrix(values[Prevector[1,],])
  AllValues<-rbind(AllValues,internalnodes)
  
  mat1 <- as.matrix(AllValues[Connections[,1],])
  mat2 <- as.matrix(AllValues[Connections[,2],])
  
  Treelength = sum(rowSums((mat1-mat2)^2))
  
  return(Treelength)
  
}