library(mnormt)

dimensions<-c(1,2,3,5,10,20,50,100)

ite<-seq(10,100,6)			#branch lengths
N=2000									#number of repetitions

combinations <- expand.grid(ite,ite)
param_mat <- apply(combinations, 2, rep, length(dimensions))
param_mat <- cbind(rep(dimensions, each=nrow(combinations)), param_mat, rep(0,nrow(param_mat)))

alpha=0.3

for (parameters in 1:nrow(param_mat)) {
  
  theta<-c(35,rep(0,param_mat[parameters,1]-1))
  
  for (repetitions in 1:N)  {
    
    positionsmatrix <- matrix(c(0), nrow = 4, ncol = param_mat[parameters,1])
    
    for (index1 in 1:(param_mat[parameters,2]/2))
      
    {positionsmatrix[1,] <- positionsmatrix[1,]+alpha*(theta-positionsmatrix[1,])+rmnorm(varcov=diag(param_mat[parameters,1]))}
    
    positionsmatrix[2,] <- positionsmatrix[1,]
    
    for (index1bis in 1:(param_mat[parameters,2]/2))
      
    {positionsmatrix[3,] <- positionsmatrix[3,]+alpha*(theta-positionsmatrix[3,])+rmnorm(varcov=diag(param_mat[parameters,1]))}
    
    positionsmatrix[4,] <- positionsmatrix[3,]
    
    for (index2 in 1:param_mat[parameters,3])
      
    {positionsmatrix[1,]=positionsmatrix[1,]+alpha*(theta-positionsmatrix[1,])+rmnorm(varcov=diag(param_mat[parameters,1]))}
    
    for (index3 in 1:param_mat[parameters,3])
      
    {positionsmatrix[2,]=positionsmatrix[2,]+alpha*(theta-positionsmatrix[2,])+rmnorm(varcov=diag(param_mat[parameters,1]))}
    
    for (index4 in 1:param_mat[parameters,3])
      
    {positionsmatrix[3,]=positionsmatrix[3,]+alpha*(theta-positionsmatrix[3,])+rmnorm(varcov=diag(param_mat[parameters,1]))}
    
    for (index5 in 1:param_mat[parameters,3])
      
    {positionsmatrix[4,]=positionsmatrix[4,]+alpha*(theta-positionsmatrix[4,])+rmnorm(varcov=diag(param_mat[parameters,1]))}
    
    o= array(c(3,-1,-1,3), c(2,2))			#int. node reconstruction 										#system equation
    
    p=matrix(c(positionsmatrix[1,]+positionsmatrix[2,],positionsmatrix[3,]+positionsmatrix[4,]),ncol=param_mat[parameters,1],byrow=T)
    
    internalnodes<-solve(o,p)
    i<-internalnodes[1,]
    j<-internalnodes[2,]
    
    T1distance= sum((positionsmatrix[1,]-i)^2)+sum((positionsmatrix[2,]-i)^2)+		#Topology 1 distance
      sum((positionsmatrix[3,]-j)^2)+sum((positionsmatrix[4,]-j)^2)+sum((i-j)^2)
    
    p=matrix(c(positionsmatrix[1,]+positionsmatrix[3,],positionsmatrix[2,]+positionsmatrix[4,]),ncol=param_mat[parameters,1],byrow=T)
    
    internalnodes<-solve(o,p)
    i<-internalnodes[1,]
    j<-internalnodes[2,]
    
    T2distance= sum((positionsmatrix[1,]-i)^2)+sum((positionsmatrix[3,]-i)^2)+		#Topology 1 distance
      sum((positionsmatrix[2,]-j)^2)+sum((positionsmatrix[4,]-j)^2)+sum((i-j)^2)
    
    p=matrix(c(positionsmatrix[1,]+positionsmatrix[4,],positionsmatrix[3,]+positionsmatrix[2,]),ncol=param_mat[parameters,1],byrow=T)
    
    internalnodes<-solve(o,p)
    i<-internalnodes[1,]
    j<-internalnodes[2,]
    
    T3distance= sum((positionsmatrix[1,]-i)^2)+sum((positionsmatrix[4,]-i)^2)+		#Topology 1 distance
      sum((positionsmatrix[3,]-j)^2)+sum((positionsmatrix[2,]-j)^2)+sum((i-j)^2)
    
    if (T1distance<T2distance && 
        T1distance<T3distance) {
      param_mat[parameters,4] <- param_mat[parameters,4]+1}
    
  }
  
}

param_mat[,4] <- (param_mat[,4]/N) *100

results <- data.frame(param_mat)

colnames(results) <- c('dimensionality', 'intern', 'termin', 'reliability')

par(mfcol= c(4,2), mai = c(0.25,1.6,0.25,1.6))

for (represent in 1:8) {
  
  plot(log(c(1:21)/c(21:1)),results[represent,],type='l',ann=FALSE,ylim=c(0,100),xaxt='n')
  lines(log(c(1:21)/c(21:1)), finalmatrix[represent+8,], lty = 2)
  axis(1,at=c(log(2/20),log(6/16),0,log(16/6),log(20/2)), labels=round(c(2/20,6/16,11/11,16/6,20/2),digits=2))
  segments(-3.5,100/3,3.5,100/3,lty=3)
  segments(-3.5,100,3.5,100,lty=3)
  legend(-3.9,105,legend=as.character(dimensions[represent]),col=c('black'),bty='n')
  
}