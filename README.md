Simulations on macroevolutionary models and phylogenetic reconstructions
for high-dimensional phenotypic data.