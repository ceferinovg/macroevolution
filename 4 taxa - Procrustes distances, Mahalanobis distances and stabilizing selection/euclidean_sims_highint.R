library(mnormt)

dimensions<-c(1,2,3,5,10,20,50,100)

n=21									#branch lengths
N=5000									#number of repetitions

combinations<-matrix(c(1:n,n:1),ncol=2)			#combine both groups
param_mat <- apply(combinations, 2, rep, length(dimensions)*2)
param_mat <- cbind(rep(dimensions, each=n*2), param_mat, rep(0,nrow(param_mat)))
graphs <- rep(c(rep('C',21), rep('H', 21)), length(dimensions))

for (parameters in 1:nrow(param_mat)) {
  
  integration <- matrix(0, ncol = param_mat[parameters,1], nrow= param_mat[parameters,1])
  
  diag(integration) <- c(0.8, rep(0.2/(param_mat[parameters,1]-1), param_mat[parameters,1]-1))
  
  for (repetitions in 1:N)  {
    
    E<-rep(0,param_mat[parameters,1])		#take 2 internal 										#nodes
    F<-rmnorm (n=1,mean=rep(0,param_mat[parameters,1]),varcov=integration*param_mat[parameters,2])
    
    
    if (graphs[parameters]=="H")
    {A<-rmnorm (n=1,mean=rep(0,param_mat[parameters,1]),varcov=integration*param_mat[parameters,2])}			#take the 4 tip nodes
    if (graphs[parameters]=="C")
    {A<-rmnorm (n=1,mean=rep(0,param_mat[parameters,1]),varcov=integration*param_mat[parameters,3])}
    
    B<-rmnorm (n=1,mean=rep(0,param_mat[parameters,1]),varcov=integration*param_mat[parameters,3])
    
    if (graphs[parameters]=="H")
    {C<-rmnorm (n=1,mean=F,varcov=integration*param_mat[parameters,2])}
    if (graphs[parameters]=="C")
    {C<-rmnorm (n=1,mean=F,varcov=integration*param_mat[parameters,3])}
    
    D<-rmnorm (n=1,mean=F,varcov=integration*param_mat[parameters,3])
    
    positionsmatrix<- rbind(A,B,C,D)
    
    o= array(c(3,-1,-1,3), c(2,2))			#int. node reconstruction 										#system equation
    
    p=matrix(c(positionsmatrix[1,]+positionsmatrix[2,],positionsmatrix[3,]+positionsmatrix[4,]),ncol=param_mat[parameters,1],byrow=T)
    
    internalnodes<-solve(o,p)
    i<-internalnodes[1,]
    j<-internalnodes[2,]
    
    T1distance= sum((positionsmatrix[1,]-i)^2)+sum((positionsmatrix[2,]-i)^2)+		#Topology 1 distance
      sum((positionsmatrix[3,]-j)^2)+sum((positionsmatrix[4,]-j)^2)+sum((i-j)^2)
    
    p=matrix(c(positionsmatrix[1,]+positionsmatrix[3,],positionsmatrix[2,]+positionsmatrix[4,]),ncol=param_mat[parameters,1],byrow=T)
    
    internalnodes<-solve(o,p)
    i<-internalnodes[1,]
    j<-internalnodes[2,]
    
    T2distance= sum((positionsmatrix[1,]-i)^2)+sum((positionsmatrix[3,]-i)^2)+		#Topology 1 distance
      sum((positionsmatrix[2,]-j)^2)+sum((positionsmatrix[4,]-j)^2)+sum((i-j)^2)
    
    p=matrix(c(positionsmatrix[1,]+positionsmatrix[4,],positionsmatrix[3,]+positionsmatrix[2,]),ncol=param_mat[parameters,1],byrow=T)
    
    internalnodes<-solve(o,p)
    i<-internalnodes[1,]
    j<-internalnodes[2,]
    
    T3distance= sum((positionsmatrix[1,]-i)^2)+sum((positionsmatrix[4,]-i)^2)+		#Topology 1 distance
      sum((positionsmatrix[3,]-j)^2)+sum((positionsmatrix[2,]-j)^2)+sum((i-j)^2)
    
    if (T1distance<T2distance && 
        T1distance<T3distance) {
      param_mat[parameters,4] <- param_mat[parameters,4]+1}
    
  }
  
}

param_mat[,4] <- (param_mat[,4]/N) *100

results <- data.frame(param_mat[,1], graphs, param_mat[,2:4])

colnames(results) <- c('dimensionality', 'branch', 'intern', 'termin', 'reliability')