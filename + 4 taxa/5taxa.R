library(mnormt)
library(phytools)

dimensions <- c(1,2,3,5,10,20,50,100)

# repetitions = replications for set of conditions
repetitions <- 1000

# create all possible trees for 5 taxa
allTrees <- allFurcTrees(5, tip.label=c('A','B','C','D','E'), to.plot=FALSE)

# store just bifurcating trees
keep<-sapply(allTrees,function(x) length(which.edge(x,c("A","B","C","D","E")))<7)
allTrees<-allTrees[!keep]

# set top branch lengths
branchlengths=21									

# now set all possible branch length combinations x repetitions
combinations<-matrix(c(1:branchlengths,branchlengths:1),ncol=2)

comb_rep <- combinations[rep(seq_len(nrow(combinations)), each = repetitions),]

# now add the dimensionalities to test
comb_rep_dim <- apply(comb_rep, 2, rep, length(dimensions))

# create a all_var matrix where all combinations of conditions are stored
all_var <- cbind(rep(dimensions, each=repetitions*nrow(combinations)), comb_rep_dim)

# make room to sequentially add phylogenetic successes or topologies distances
all_var <- cbind(all_var,rep(0,nrow(all_var)),rep(0,nrow(all_var)))

colnames(all_var) <- c('dimensions', 'central.branch', 
'term.branch', 'Success', 'Dist.topology')

# set phylogeny to simulate: a caterpillar topology
TrueTree <- unroot(read.tree(text = "(((A,B),E),(C,D));"))

# now loop for each set of conditions testing the success or failure
# of the phylogenetic method in those conditions

for (variable in 1:nrow(all_var)) {
  
  # being F = (1,1...) and the starting point of the simulation
  # simulate sequentially the position of the other nodes
  # according to one combination of dimensionality and branch lengths
  
  centre <- rep(1, all_var[variable,1])
  ij <- as.matrix(rmnorm (n=2,mean=centre, 
                          varcov=(diag(all_var[variable,1])*all_var[variable,2])))
  E <- as.matrix(rmnorm (n=1,mean=centre, 
                         varcov=(diag(all_var[variable,1])*all_var[variable,3])))
  AB <- as.matrix(rmnorm (n=2,mean=ij[1,], 
                          varcov=(diag(all_var[variable,1])*all_var[variable,3])))
  CD <- as.matrix(rmnorm (n=2,mean=ij[2,], 
                          varcov=(diag(all_var[variable,1])*all_var[variable,3])))
  
  # store terminal values in a matrix
  values <- rbind(AB[1,],AB[2,],CD[1,],CD[2,],c(E))
  rownames(values) <- c('A','B','C','D','E')
  
  # obtain the internal nodes for the true topology
  # by applying McArdle and Rodrigo 94
  MAI <- matrix(c(3,-1,0,-1,3,-1,0,-1,3), 
                byrow=T, ncol=3,nrow=3)
  Y <- matrix(c(AB[1,]+AB[2,],E,CD[1,]+CD[2,]),ncol= all_var[variable,1],byrow=T)
  
  internalnodes<-solve(MAI,Y)
  
  # obtain total distance of the phylogenetic reconstruction
  T1distance = sqrt(sum((AB[1,] - internalnodes[1,])^2)) +
    sqrt(sum((AB[2,] - internalnodes[1,])^2)) +
    sqrt(sum((internalnodes[1,] - internalnodes[2,])^2)) +
    sqrt(sum((CD[1,] - internalnodes[3,])^2)) +
    sqrt(sum((internalnodes[2,] - internalnodes[3,])^2)) +
    sqrt(sum((internalnodes[2,] - E)^2)) +
    sqrt(sum((CD[2,] - internalnodes[3,])^2))
  
  # now apply squared-change parsimony to all the possible trees
  treelengths <- sapply(allTrees, buildtree.gen)
  
  # if the first shortest tree isn't equal to the true tree
  # store the topological distance from the shortest tree to the true one
  if (!all.equal(allTrees[[which(min(treelengths)[1]==treelengths)[1]]], TrueTree))
    
  {all_var[variable,5] <- dist.topo(allTrees[[which(treelengths==min(treelengths))[1]]], TrueTree)}	
  
  # if they both are the same tree (the shortest and the true), 
  # then store a success of the phylogenetic method
  if (all.equal(allTrees[[which(min(treelengths)[1]==treelengths)[1]]], TrueTree))
    
  {all_var[variable,4] <- 1}	
  
}
